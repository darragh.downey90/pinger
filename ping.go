package main

import (
	"encoding/json"
	"log"
	"os"
	"time"

	zmq "github.com/pebbe/zmq4"
)

// Ping function to substitute for CRON
// sends dateFrom dateTo pings to some ZMQ server
// should be spun
func Ping() {
	// see https://golang.org/pkg/time/#ParseDuration on acceptable input
	interval := os.Getenv("INTERVAL")
	// must be a valid address for zeromq to bind to
	connStr := os.Getenv("WORKURI")
	// Frequency of pings; currently not used other than debugging
	freq := os.Getenv("FREQUENCY")
	// test string format; either "na" or a valid string as documented here https://golang.org/pkg/time/#Parse
	testPingFmt := os.Getenv("TESTPINGF")
	// must be either "na" or a valid string as documented here https://golang.org/pkg/time/#Parse
	testPingStart := os.Getenv("TESTPINGS")
	// must be either "na" or a valid string as documented here https://golang.org/pkg/time/#Parse
	testPingEnd := os.Getenv("TESTPINGE")

	log.Printf("I: Acquiring socket")
	req, err := zmq.NewSocket(zmq.REQ)
	if err != nil {
		log.Printf("E: Failed to register a REQ socket! %v\n", err)
		return
	}
	defer req.Close()
	log.Printf("I: Socket acquired - Binding to URI")

	err = req.Connect(connStr)
	if err != nil {
		log.Printf("E: Failed to bind to URI %v", err)
		return
	}

	log.Printf("I: Bound to URI - %s", connStr)

	intervalDuration, err := time.ParseDuration(interval)
	if err != nil {
		log.Printf("E: Invalid format for duration %v\n", err)
		return
	}

	// ch := make(chan string)

	// For testing based on docker configuration input
	// users must either provide "na" for not used or a valid formatted time string
	if testPingFmt != "na" && testPingFmt != "" {
		startTime, err := time.Parse(testPingFmt, testPingStart)
		if err != nil {
			log.Printf("E: Could not parse test start time: %v\n", err)
			return
		}
		endTime, err := time.Parse(testPingFmt, testPingEnd)
		if err != nil {
			log.Printf("E: Could not parse test end time: %v\n", err)
			return
		}
		log.Printf("I: Running test")
		go testPing(startTime, endTime, intervalDuration, freq, req)
	} else {
		log.Printf("I: Running in production")
		go ping(intervalDuration, freq, req)
	}
	// listen for values from ping
	for {
		//v, ok := <-ch
		//if ok == false {
		//	log.Printf("W: Breaking listening loop due to some issue in ping\n")
		//	break
		//}
		//log.Printf("I: Received %v %v\n", v, ok)
	}
}

func ping(interval time.Duration, freq string, req *zmq.Socket) {
	for {
		now := time.Now()
		lastPoll := now.Add(-interval)

		structuredMsg := Message{
			DateTimeFrom: lastPoll.Unix(),
			DateTimeTo:   now.Unix(),
			Frequency:    freq,
			Line:         os.Getenv("LINE"),
		}

		bmsg, err := json.Marshal(structuredMsg)
		if err != nil {
			log.Printf("E: Failed to marshal message to JSON %v", err)
			return
		}
		log.Printf("I: Sending - %v", bmsg)
		req.SendBytes(bmsg, 0)

		reply, err := req.RecvMessage(0)
		if err != nil {
			log.Printf("E: Failed to parse response: %v\n", err)
		} else {
			log.Printf("I: Received response! %v\n", reply)
		}

		log.Printf("I: Sleeping for %v seconds\n", interval.Seconds())
		time.Sleep(interval)
	}
}

func testPing(startTime, endTime time.Time, interval time.Duration, msg string, req *zmq.Socket) {
	// simulate time.Now() call
	now := startTime
	for now.Unix() < endTime.Unix() {
		lastPoll := now.Add(-interval)

		// msg is unused for the moment

		structuredMsg := Message{
			DateTimeFrom: lastPoll.Unix(),
			DateTimeTo:   now.Unix(),
			Frequency:    "Test",
			Line:         os.Getenv("LINE"),
		}

		bmsg, err := json.Marshal(structuredMsg)
		if err != nil {
			log.Printf("E: Failed to marshal message to JSON %v", err)
			return
		}
		log.Printf("I: Sending - %v", bmsg)
		req.SendBytes(bmsg, 0)

		reply, err := req.Recv(0)
		if err != nil {
			log.Printf("E: Failed to parse response: %v\n", err)
		} else {
			log.Printf("I: Received response! %v\n", reply)
		}

		log.Printf("I: Sleeping for %v seconds\n", interval.Seconds())
		time.Sleep(interval)
		// simulate time.Now() call
		now = now.Add(interval)
	}
}
