package main

// Message to pass between containers
// original message:
// 	hourly %s daily %s dateFrom %v dateTo %v
type Message struct {
	DateTimeFrom int64
	DateTimeTo   int64
	Frequency    string
	Line         string
}
