package main

import (
	"log"
	"os"
	"testing"

	zmq "github.com/pebbe/zmq4"
)

// TestPing spins up a simple server to ensure pings are being sent/received
func TestPing(t *testing.T) {
	var messages = []struct {
		serverConnStr string
		clientConnStr string
		message       string
		interval      string
	}{
		{
			"tcp://*:4444",
			"tcp://127.0.0.1:4444",
			"Howdy!",
			"10s",
		},
		// *expects 10s/10h/10m/10u etc.
	}

	for _, item := range messages {
		// spin up a worker to ensure the message is being sent correctly
		go testServer(item.serverConnStr)
		// server is listening
		err := os.Setenv("WORKURI", item.clientConnStr)
		if err != nil {
			log.Printf("Failed to set WORKURI env %v\n", err)
		}
		err = os.Setenv("MESSAGE", item.message)
		if err != nil {
			log.Printf("Failed to set MESSAGE env %v\n", err)
		}
		err = os.Setenv("INTERVAL", item.interval)
		if err != nil {
			log.Printf("Failed to set INTERVAL env %v\n", err)
		}

		Ping()
	}
}

func testServer(connStr string) {
	rep, err := zmq.NewSocket(zmq.REP)
	if err != nil {
		log.Printf("Failed to bind port for test server! %v\n", err)
	}

	defer rep.Close()
	rep.Bind(connStr)

	for {
		msg, err := rep.Recv(0)
		if err != nil {
			log.Printf("Error in parsing message from Pinger! %v\n", err)
		}

		log.Printf("Message received was %v\n", msg)

		reply := "ACK"
		rep.Send(reply, 0)
		log.Printf("Sent %v\n", reply)
	}
}
